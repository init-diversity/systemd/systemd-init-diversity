# systemd-init-diversity

## systemd with systemd-shim support for debian based multi-init distro's.

All credits for maintaining this fork need to go to the MX Linux development team.

Official MX systemd git repo at:

- https://github.com/knelsonmeister/systemd
- https://github.com/mx-linux/systemd

This repo aims to provide the latest up-to-date systemd source that that has support for systemd-shim.

## Background:
Systemd-shim allows a system to boot up using a SYSV or other inits, but still be able to support the modern programs that depend on systemd.  
It is not perfect, but it allows for a Linux distribution to support init-diversity with systemd as a choice in the grub boot menu.  
Debian used to support systemd-shim, but support was dropped in Debian Buster. 

Current "init-diversity" systemd-shim repo at:
- https://gitlab.com/init-diversity/systemd/systemd-shim.git

Official MX systemd-shim repo at

- https://github.com/knelsonmeister/systemd-shim
- https://github.com/mxlinux/systemd-shim

## Current Version: 1:252.6-1
Based on: https://mxrepo.com/mx/repo/pool/main/s/systemd/systemd_252.6-1mx23+1.debian.tar.xz

## Changes:
  - Propped forward 4 patches from old systemd that were removed when systemd-shim was removed
  - Updated debian/patches/series file to add the 4 patches
  - Updated debian/control to modify dependency for libpam-systemd to allow systemd-shim or systemd-sysv
  - Restored old user runtime path functionality
  - Removed confict with Consolekit to allow it to be used with alternate inits.

## How To Build:
NOTE: The tests phase of the compile will fail if compiling on a system not running systemd as init.  

The recommended method to build this package directly from git on debian or MX while booted on systemd 
```
sudo apt install git-builpackage #alongside Build-Depends
gbp clone https://gitlab.com/init-diversity/systemd/systemd-init-diversity.git && cd systemd-init-diversity && gbp buildpackage`
```

If compiling using antiX or Devuan or simply do not prefer to compile while booted in systemd you will need to set-up a chroot (recommended method):
```
sudo apt install git-buildpackage pbuilder cowbuilder qemu-user-static &&
sudo DIST=stable ARCH=amd64 git-pbuilder create &&
sudo DIST=stable ARCH=amd64 git-pbuilder login --save-after-login
```
While in chroot:
```
apt update && apt upgrade && exit
```
Once the this has been created, a non-systemd user can build using:

```
gbp clone https://gitlab.com/init-diversity/systemd/systemd-init-diversity.git && 
cd systemd-init-diversity && 
gbp buildpackage --git-pbuilder --git-dist=stable --git-arch=amd64 -uc -us
```